public class Staff extends Manager{

	public Staff(String nama, int gajiAwal){
		super(nama, gajiAwal);
		setTipe("STAFF");
	}

	public void addBawahan(Karyawan pekerja){
		if (bawahan.size()>10){
			System.out.println( "Anda tidak layak memiliki bawahan");
		}
		else{
			if(findBawahan(pekerja.nama) == null){
				if(pekerja instanceof Staff){
					bawahan.add(pekerja);
					System.out.println("Karyawan " + pekerja.getNama()+ " berhasil ditambahkan menjadi bawahan " + nama);
				}
				else{
					System.out.println( "Anda tidak layak memiliki bawahan");
				}
			}
			else{
				System.out.println( "Karyawan " + pekerja.getNama()+ " telah menjadi bawahan " + nama);
			}
		}
	}
	public boolean canPromote(){
		if(gajiAwal >maxGajiStaff){
			return true;
		}
		else{
			return false;
		}
	}
}