public class Intern extends Staff{
	
	public Intern(String nama, int gajiAwal){
		super(nama,gajiAwal);
		setTipe("INTERN");
	}

	public void addBawahan(Karyawan pekerja){
		System.out.println( "Anda tidak layak memiliki bawahan");
	}

}