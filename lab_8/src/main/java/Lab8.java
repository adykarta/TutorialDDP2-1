import java.util.Scanner;
public class Lab8 {
    public static void main(String[] args) {
    	Scanner masukan = new Scanner (System.in);
    	Korporasi korp = new Korporasi("PT. TAMPAN");
    	Karyawan.setMaxGajiStaff(Integer.parseInt(masukan.nextLine()));
    	String input[];

    	while(true){
    		input = masukan.nextLine().toUpperCase().split(" ");
    		switch(input[0]){
    			case("TAMBAH_KARYAWAN"):
    				korp.addKaryawan(input[1],input[2],Integer.parseInt(input[3]));
    				break;
    			case("STATUS"):
    				korp.status(input[1]);
    				break;
    			case("TAMBAH_BAWAHAN"):
    				korp.addBawahan(input[1],input[2]);
    				break;
    			case("GAJIAN"):
    				korp.gajian();
    				break;
    			case("EXIT"):
    				return;
    			default:
    				System.out.println("Tidak ada perintah");


    		}

    	}

    }

}