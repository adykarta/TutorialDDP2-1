public class Manager extends Karyawan{
	
	public Manager(String nama, int gajiAwal){
		super(nama,gajiAwal);
		setTipe("MANAGER");

	}

	public void addBawahan(Karyawan pekerja){
		if(bawahan.size()>10){
			System.out.println("Anda tidak layak memiliki bawahan");
		}
		else{
			if(findBawahan(pekerja.nama)==null){
				if(pekerja instanceof Manager){
					bawahan.add(pekerja);
					System.out.println( "Karyawan " + pekerja.getNama()+ " berhasil ditambahkan menjadi bawahan " + nama);
				}
				else{
					System.out.println( "Anda tidak layak memiliki bawahan");
				}

			}
			else{
				System.out.println( "Karyawan " + pekerja.getNama() + " telah menjadi bawahan " + nama);
			}


		}

	}

	public boolean canPromote(){
		return false;
	}

}