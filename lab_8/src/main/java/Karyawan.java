import java.util.ArrayList;

abstract class Karyawan{
	protected String nama;
	protected int gajiAwal;
	protected String tipe;
	protected int counter;
	static int maxGajiStaff = 18000;
	protected ArrayList <Karyawan> bawahan = new ArrayList <Karyawan>();

	public Karyawan(String nama, int gajiAwal){
		this.nama = nama.substring(0,1).toUpperCase() + nama.substring(1).toLowerCase();
		this.gajiAwal = gajiAwal;
	}

	public void setNama(String nama){
		this.nama = nama;
	}


	public String getNama(){
		return this.nama;
	}

	public static void setMaxGajiStaff(int maxGaji){
		maxGajiStaff = maxGaji;
	}
	public void setTipe(String tipe){
		this.tipe = tipe;
	}

	public String getTipe(){
		return this.tipe;
	}

	public ArrayList<Karyawan> getBawahan(){
		return bawahan;
	}

	public void setBawahan(ArrayList<Karyawan> bawahan){
		this.bawahan =bawahan;
	}
	public void setGajiAwal(int gajiAwal){
		this.gajiAwal = gajiAwal;
	}

	public void setCounter(int counter){
		this.counter = 0;
	}

	public int getGajiAwal(){
		return this.gajiAwal;
	}

	public Karyawan findBawahan(String nama){
		for(Karyawan a: bawahan){
			if(a.nama.equalsIgnoreCase(nama)){
				return a;
			}
		}
		return null;
	}

	public void gaji(){
		if(counter<5){
			counter+=1;
		}
		else if(counter==5){
			int gajiBaru = (int)(gajiAwal + gajiAwal *0.1);
			System.out.println(nama +" mengalami kenaikan gaji sebesar 10% dari " + gajiAwal + " menjadi " + gajiBaru);
			this.setGajiAwal(gajiBaru);
			this.setCounter(0);

		}

	}

	public abstract boolean canPromote();



	public abstract void addBawahan(Karyawan bawahan);


}

