import java.util.ArrayList;
public class Korporasi{
	protected String name;
	protected ArrayList<Karyawan> karyawan = new ArrayList<Karyawan>();

	public Korporasi(String name){
		this.name = name;
	}
	public Karyawan findKaryawan(String nama){
		for(Karyawan a: karyawan){
			if(a.getNama().equalsIgnoreCase(nama)){
				return a;
			}
		}
		return null;
	}

	public void addKaryawan(String tipe, String nama, int gajiAwal){
		if(karyawan.size()>1000){
			System.out.println("Jumlah karyawan melebihi kapasitas");

		}
		else{
			if(this.findKaryawan(nama)!= null){
				System.out.println( nama + " sudah bekerja di sini");
			}
			else{
				if(tipe.equalsIgnoreCase("MANAGER")){
					karyawan.add(new Manager(nama,gajiAwal));
				}
				else if(tipe.equalsIgnoreCase("STAFF")){
					karyawan.add(new Staff(nama,gajiAwal));
				}
				else if(tipe.equalsIgnoreCase("INTERN")){
					karyawan.add(new Intern(nama,gajiAwal));
				}
			}
			System.out.println( nama + " mulai bekerja sebagai " + tipe + " di PT. TAMPAN");
		}

	}

	public void addBawahan(String atasan, String bawahan){
		Karyawan boss = findKaryawan(atasan);
		Karyawan bawah = findKaryawan(bawahan);
		if(boss== null || bawah==null){
			System.out.println("Nama tidak berhasil ditemukan");
		}
		else{
			boss.addBawahan(bawah);
		}
	}

	public void status(String nama){
		if(this.findKaryawan(nama)==null){
			System.out.println("Nama tidak terdaftar");
		}
		else{
			System.out.println(nama + " " + findKaryawan(nama).getGajiAwal());
		}
	}

	public void gajian(){
		for(Karyawan a: karyawan){
			a.gaji();
		}
		System.out.println("Semua karyawan telah diberikan gaji");
		for(int i = 0; i < karyawan.size(); i++){
			Karyawan a = karyawan.get(i);
			if(a.canPromote()){
				ArrayList<Karyawan> temp1 = a.getBawahan();
				Manager upgrade = new Manager(a.getNama(),a.getGajiAwal());
				upgrade.setBawahan(temp1);
				karyawan.set(i,upgrade);
				System.out.println("Selamat, " + a.getNama() + " telah dipromosikan menjadi MANAGER");

			}
		}

	}

		

}