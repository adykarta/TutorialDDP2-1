package lab9.event;

import java.lang.Object;
import java.text.*;
import java.util.Date;
import java.time.*;
import java.lang.Comparable;
import java.math.BigInteger;

// A class representing an event and its properties

public class Event implements Comparable <Event>
{
    /** Name of event */
    private String name;
    Date startTime;
    Date endTime;
    BigInteger cost;
    SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-MM-dd_HH:mm:ss");
    // TODO: Make instance variables for representing beginning and end time of event
    
    // TODO: Make instance variable for cost per hour
    
    // TODO: Create constructor for Event class
    
    /**
    * Accessor for name field. 
    * @return name of this event instance
    */
    public Event(String name, String startTime, String endTime, String cost){
        this.name = name;
        try{
            this.startTime = dateFormat.parse(startTime);
            this.endTime = dateFormat.parse(endTime);
        }
        catch(Exception e){}
        this.cost = new BigInteger(cost);
    }

    public String getName()
    {
        return this.name;
    }

    public Date getStartTime(){
        return this.startTime;
    }

    public Date getEndTime(){
        return this.endTime;
    }

    public BigInteger getCost(){
        return this.cost;
    }



    public boolean overlapsWith(Event other){
        if(other.startTime.after(this.startTime) && other.startTime.before(this.endTime)){
            return true;
        }
        else if(other.endTime.after(this.startTime)&&other.endTime.before(this.endTime)){
            return true;
        }
        return false;

    }
    public int compareTo(Event other){
        return getStartTime().compareTo(other.getStartTime());
    }

    public String toString(){
        SimpleDateFormat dateFormatOut = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");
        return (name + "\nWaktu mulai: " + dateFormatOut.format(this.startTime) + "\nWaktu selesai: " + dateFormatOut.format(this.endTime) + "\nBiaya kehadiran: " + cost);
    }
    
    // TODO: Implement toString()
    
    // HINT: Implement a method to test if this event overlaps with another event
    //       (e.g. boolean overlapsWith(Event other)). This may (or may not) help
    //       with other parts of the implementation.
}
