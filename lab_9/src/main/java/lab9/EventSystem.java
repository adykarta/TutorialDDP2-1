package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.util.ArrayList;
import java.lang.Object;
import java.text.*;
import java.util.Date;
import java.util.Arrays;
import java.math.BigInteger;

/**
* Class representing event managing system
*/
public class EventSystem
{
    /**
    * List of events
    */
    private ArrayList<Event> events;
    
    /**
    * List of users
    */
    private ArrayList<User> users;
    
    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem()
    {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }
    
    public Event getEvent(String name){
        for(Event a: events){
            if(a.getName().equalsIgnoreCase(name)){
                return a;
            }
        }
        return null;
    }

    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr)
    {
        if(this.getEvent(name) != null){
            return "Event " + name + " sudah ada!";
        }
        else{
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
            try{
                Date startTime = dateFormat.parse(startTimeStr);
                Date endTime = dateFormat.parse(endTimeStr);
                if(startTime.after(endTime)){
                    return "Waktu yang diinputkan tidak valid!";

                }
                else{
                    events.add(new Event(name,startTimeStr,endTimeStr,costPerHourStr));
                    return "Event " + name + " berhasil ditambahkan!";
                
                }
            }
            catch(Exception e){
                return ("Format salah!");
            }


        }
        // TODO: Implement!
        
    }

    public User getUser(String name){
        for(User a: users){
            if(a.getName().equalsIgnoreCase(name)){
                return a;
            }
        }
        return null;
    }
    
    public String addUser(String name)
    {
        if(this.getUser(name) != null){
            return "User " + name + " sudah ada!";          
        }
        else{
            users.add(new User(name));
            return "User " + name + " berhasil ditambahkan!";
        }

    }
    
    public String registerToEvent(String userName, String eventName)
    {
       
        if(this.getUser(userName)== null && this.getEvent(eventName) == null ){
            return "Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!";

        }
        else if(getUser(userName)==null){
            return "Tidak ada pengguna dengan nama " + userName + "!";
        }
        else if(getEvent(eventName)==null){
            return "Tidak ada event dengan nama " + eventName + "!";
        }
        else{
            if( getUser(userName).addEvent(getEvent(eventName))){
                getUser(userName).getEvents().add(getEvent(eventName));
                return userName + " berencana menghadiri " + eventName + "!";
                
            }
            else{
                return userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
               
            }
        }

        
    }
}