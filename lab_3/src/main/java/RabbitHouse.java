//Masukkan RabbitHouse.java Anda di dalam folder ini
import java.util.Scanner;
public class RabbitHouse{
	
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		System.out.print("Masukan nama: ");
		String huruf = input.nextLine();
		int jumlah = huruf.split(" ")[1].length();
		if(jumlah>10){
			System.out.println("Warning:Panjang nama tidak boleh lebih dari 10!");
			System.exit(0);
		}
		
		System.out.print(kelinci(jumlah));
	}
	
	public static int kelinci(int jumlah){
		int hasil = 1;
		if(jumlah<=1){
			return 1;
		}
		else{
			hasil += jumlah * kelinci(jumlah - 1);
			return hasil;
		}
			
	}
	
}