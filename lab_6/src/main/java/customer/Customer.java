package customer;
import theater.*;
import movie.*;
import ticket.*;
import java.util.ArrayList;

public class Customer{
	private String nama;
	private int umur;
	private String kelamin;

	public Customer(String nama, String kelamin, int umur){
		this.nama = nama;
		this.umur = umur;
		this.kelamin = kelamin;
	}

	public String getNama(){
		return nama;
	}
	public void setNama(String nama){
		this.nama = nama;
	}
	public int getUmur(){
		return umur;
	}
	public void setUmur(int umur){
		this.umur = umur;
	}
	public String getKelamin(){
		return kelamin;
	}
	public void setKelamin(String kelamin){
		this.kelamin = kelamin;

	}

	public Ticket orderTicket(Theater bioskop, String film, String jadwal, String tipe){
		for(Ticket ticket : bioskop.getTiketTersedia()){
	     	Movie movie = ticket.getFilm();
	      	String rating = movie.getRating();
	      	if(movie.getJudul().equals(film) && ticket.getJadwal().equals(jadwal) && ticket.getTipe().equals(tipe)){
	        	if(rating.equals("Umum")){
	          	System.out.println(nama+" telah membeli tiket "+film+" jenis "+tipe+" di "+bioskop.getNamaBioskop()+" pada hari "+jadwal+" seharga Rp. "+ticket.hargaTiket());
	          	bioskop.masukanSaldo(ticket.hargaTiket());
	          	return ticket;
	          	}
	        if(rating.equals("Remaja")){
	          	if(umur>=13){
	        		System.out.println(nama+" telah membeli tiket "+film+" jenis "+tipe+" di "+bioskop.getNamaBioskop()+" pada hari "+jadwal+" seharga Rp. "+ticket.hargaTiket());
	            	bioskop.masukanSaldo(ticket.hargaTiket());
	            	return ticket;
	          	}
	          	else{
	            	System.out.println(nama+" masih belum cukup umur untuk menonton "+film+" dengan rating "+rating);
	            	return null;
	          	}
	        }
	        if(rating.equals("Dewasa")){
	        	if(umur>=17){
	            	System.out.println(nama+" telah membeli tiket "+film+" jenis "+tipe+" di "+bioskop.getNamaBioskop()+" pada hari "+jadwal+" seharga Rp. "+ticket.hargaTiket());
	            	bioskop.masukanSaldo(ticket.hargaTiket());
	            	return ticket;
	          	}
	          	else{
	            	System.out.println(nama+" masih belum cukup umur untuk menonton "+film+" dengan rating "+rating);
	            	return null;
	          	}
	        }
	    	}
	      
    	}
    	System.out.println("Tiket untuk film "+film+" jenis "+tipe+" dengan jadwal "+jadwal+" tidak tersedia di "+bioskop.getNamaBioskop());
    	return null;
	
	}

	public void findMovie(Theater bioskop, String film){
    	Movie[] movie = bioskop.getDaftarFilm();
    	int panjang = movie.length;
    	for(int a=0; a<panjang;a++){
      		if(film.equals(movie[a].getJudul())){
        		System.out.println(movie[a]);
        		return;
        	}
      	}
    
    	System.out.println("Film "+film+" yang dicari "+nama+" tidak ada di bioskop "+bioskop.getNamaBioskop());
    	return;
  	}
}