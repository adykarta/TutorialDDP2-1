package theater;
import customer.*;
import movie.*;
import ticket.*;
import java.util.ArrayList;

public class Theater{
	private String namaBioskop;
	private Movie[] daftarFilm;
	private int saldoKas;
	private ArrayList<Ticket> tiketTersedia;

	public Theater(String namaBioskop,int saldoKas,  ArrayList<Ticket> tiketTersedia,Movie[] daftarFilm ){
		this.namaBioskop = namaBioskop;
		this.daftarFilm = daftarFilm;
		this.tiketTersedia = tiketTersedia;
		this.saldoKas = saldoKas;
	}
	public void masukanSaldo(int tambahan){
		this.saldoKas+=tambahan;
	}
	public String getNamaBioskop(){
		return namaBioskop;
	}
	public void setNamaBioskop(String namaBioskop){
		this.namaBioskop = namaBioskop;
	}
	public Movie[] getDaftarFilm(){
		return daftarFilm;
	}
	public void setDaftarFilm(Movie[] daftarFilm){
		this.daftarFilm = daftarFilm;
	}
	public ArrayList<Ticket> getTiketTersedia(){
		return tiketTersedia;
	}
	public void setTiketTersedia(ArrayList<Ticket> tiketTersedia){
		this.tiketTersedia = tiketTersedia;
	}
	public int getSaldoKas(){
		return saldoKas;
	}
	public void setSaldoKas(int saldoKas){
		this.saldoKas = saldoKas;
	}
	public String getListFilm(){
		String list = "";
		for (int i=0; i<getDaftarFilm().length -1; i++){
			list += getDaftarFilm()[i].getJudul() + ", ";
		} 
		list += getDaftarFilm()[getDaftarFilm().length-1].getJudul();

		return list;
	}
	public static void printTotalRevenueEarned(Theater[] bioskop){
		int totalpendapatan = 0;
		for (int i =0; i< bioskop.length; i ++ ){
			totalpendapatan+=bioskop[i].getSaldoKas();
		}
		System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + totalpendapatan);
		System.out.println ("------------------------------------------------------------------");
		for (int j = 0; j<bioskop.length;j++){
			if(j==bioskop.length-1){ 
       			System.out.println("Bioskop     : "+bioskop[j].getNamaBioskop()); 
        		System.out.println("Saldo Kas   : Rp. "+bioskop[j].getSaldoKas()); 
        		System.out.println(""); 
        		System.out.println("------------------------------------------------------------------"); 
      		} 
     		else{ 
      			System.out.println("Bioskop     : "+bioskop[j].getNamaBioskop()); 
      			System.out.println("Saldo Kas   : Rp. "+bioskop[j].getSaldoKas()); 
      			System.out.println(""); 
      		}
		}

	}
	public void printInfo(){
		String str = "------------------------------------------------------------------\n" + 
				"Bioskop                 : " + this.getNamaBioskop() + "\n" +
				"Saldo Kas               : " + this.getSaldoKas() + "\n" +
				"Jumlah tiket tersedia   : " + this.getTiketTersedia().size() + "\n" +
				"Daftar Film tersedia    : " + this.getListFilm() + "\n" +
				"------------------------------------------------------------------";
		System.out.println(str);

	}
}