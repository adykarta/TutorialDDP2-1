package movie;

public class Movie{
	private String judul;
	private String genre;
	private int durasi;
	private String rating;
	private String jenis;

	public Movie(String judul, String rating, int durasi, String genre, String jenis){
		this.judul = judul;
		this.genre = genre;
		this.durasi = durasi;
		this.rating = rating;
		this.jenis = jenis;
	}
	public String getJudul(){
		return judul;
	}
	public void setJudul(String judul){
		this.judul = judul;
	}
	public String getGenre(){
		return genre;
	}
	public void setGenre(String genre){
		this.genre = genre;
	}
	public int getDurasi(){
		return durasi;
	}
	public void setDurasi(int Durasi){
		this.durasi = durasi;
	}
	public String getRating(){
		return rating;
	} 
	public void setRating(String rating){
		this.rating = rating;
	}
	public String getJenis(){
		return jenis;
	}
	public void setJenis(String jenis){
		this.jenis = jenis;
	}
	public String toString(){
		return "------------------------------------------------------------------\n"+
		"Judul   : " + this.getJudul() +"\n"+
		"Genre   : " + this.getGenre() +"\n"+
		"Durasi  : " + this.getDurasi() + " menit\n"+
		"Rating  : " + this.getRating() +"\n"+
		"Jenis   : Film " + this.getJenis() + "\n"+
		"------------------------------------------------------------------";

	} 
}