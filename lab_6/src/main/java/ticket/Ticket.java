package ticket;
import theater.*;
import movie.*;
import customer.*;

public class Ticket{
	private Movie film;
	private String jadwal;
	private boolean statusTiket;
	private String tipe;
	private Theater bioskop;
	
	public Ticket(Movie film, String jadwal, boolean statusTiket){
		this.film = film;
		this.jadwal = jadwal;
		this.statusTiket = statusTiket;
		if(statusTiket){
			this.tipe = "3 Dimensi";
		}
		else{
			this.tipe = "Biasa";
		}
	}

	public Movie getFilm(){
		return film;
	}
	public void setFilm(Movie film){
		this.film = film;
	}
	public String getJadwal(){
		return jadwal;
	}
	public void setJadwal(String jadwal){
		this.jadwal = jadwal;
	}
	public boolean getStatusTiket(){
		return statusTiket;
	}
	public void setStatusTiket(boolean statusTiket){
		this.statusTiket = statusTiket;
	}
	public String getTipe(){
		return tipe;
	}
	public void setTipe(String tipe) {
		this.tipe = tipe;
	}
	public Theater getBioskop(){
		return bioskop;
	}
	public void setBioskop(Theater bioskop){
		this.bioskop = bioskop;
	}

	public int hargaTiket(){
		int biaya = 60000;
		if(this.jadwal.equals("Sabtu")|| this.jadwal.equals("Minggu")){
			biaya = biaya + 40000;
		}
		if(this.getStatusTiket()==true){
			biaya = biaya + (biaya*20/100);
		}

		return biaya;

	}
	public String toString(){
		return "------------------------------------------------------------------\n"+
				"Film            : " + this.getFilm().getJudul() + "\n" +
				"Jadwal Tayang   : " + this.getJadwal() + "\n" +
				"Jenis           : " + this.getTipe() + "\n" +
				"------------------------------------------------------------------";
	}

}