//Masukkan Manusia.java Anda di dalam folder ini
public class Manusia{
	private String nama;
	private int umur;
	private int uang;
	private float kebahagiaan;
	private final static float kebahagiaan_min = 0;
	private final static float kebahagiaan_max = 100;
	private final static float kebahagiaan_dasar = 50;
	private final static int uang_dasar = 50000;
	private final static int umur_min = 18;
	
	public Manusia (String nama, int umur, int uang, float kebahagiaan){
		this.nama=nama;
		this.umur=umur;
		this.uang=uang;
		this.kebahagiaan=kebahagiaan;
	}
	public Manusia (String nama, int umur){
		this.nama=nama;
		this.umur=umur;
		this.uang= uang_dasar;
		this.kebahagiaan = kebahagiaan_dasar;
		
	}
	
	public Manusia (String nama, int umur, int uang){
		this.nama=nama;
		this.umur=umur;
		this.uang=uang;
		this.kebahagiaan=kebahagiaan_dasar;
		
	}
	public String getNama(){
		return nama;
	}
	public void setNama(String nama){
		this.nama=nama;
	}
	public int getUmur(){
		return umur;
	}
	public void setUmur(int umur){
		this.umur=umur;
	}
	public int getUang(){
		return uang;
	}
	public void setUang(int uang){
		this.uang=uang;
	}
	public float getKebahagiaan(){
		return kebahagiaan;
	}
	public void setKebahagiaan(float kebahagiaan){
		if(kebahagiaan > kebahagiaan_max){
			this.kebahagiaan = kebahagiaan_max;
		}
		else if( kebahagiaan < kebahagiaan_min){
			this.kebahagiaan = kebahagiaan_min;
		}
		else{
			this.kebahagiaan=kebahagiaan;
		}
	}
	
	public void beriUang(Manusia penerima){
		int jumlahAscii = 0;
		for(int a=0; a < penerima.nama.length() ; a++){
			char x = penerima.nama.charAt(a);
			int ascii = (int)(x);
			jumlahAscii+=ascii;
		
		}
		int jumlahUang = jumlahAscii*100;
		if(jumlahUang>this.uang){
			System.out.println(this.nama + " ingin memberikan uang kepada " + penerima.nama + "namun tidak memiliki cukup uang :(");
			
		}
		else{
			float kebahagiaan_baru = (this.kebahagiaan + ((float)jumlahUang/6000));
			this.setKebahagiaan(kebahagiaan_baru);
			System.out.println(this.nama + " memberikan uang sebanyak " + jumlahUang + " kepada " + penerima.nama + ", mereka berdua senang :D");
			this.uang-=jumlahUang;
			penerima.uang +=jumlahUang;
			penerima.setKebahagiaan((float)(penerima.kebahagiaan + ((float) jumlahUang/6000)));
		}
	}
	
	public void beriUang(Manusia penerima, int jumlahUang){
		if(jumlahUang>this.uang){
			System.out.println(this.nama + " ingin memberikan uang kepada " + penerima.nama + "namun tidak memiliki cukup uang :(");
			
		}
		else{
			float kebahagiaan_baru = (this.kebahagiaan + ((float)jumlahUang/6000));
			this.setKebahagiaan(kebahagiaan_baru);
			System.out.println(this.nama + " memberikan uang sebanyak " + jumlahUang + " kepada " + penerima.nama + ", mereka berdua senang :D");
			this.uang-=jumlahUang;
			penerima.uang +=jumlahUang;
			penerima.setKebahagiaan((float)(penerima.kebahagiaan + ((float) jumlahUang/6000)));
		}
		
	} 
	
	public void bekerja(int durasi, int bebanKerja){
		if (this.umur< umur_min){
			System.out.println(this.nama + " belum boleh bekerja karena masih di bawah umur D:");
		}
		else{
			int bebanKerjaTotal = durasi * bebanKerja;
			int pendapatan = 0 ;
			
			if(bebanKerjaTotal<=kebahagiaan){
				this.setKebahagiaan(kebahagiaan-bebanKerjaTotal);
				pendapatan = bebanKerjaTotal *10000;
				System.out.println(this.nama + " bekerja full time, total pendapatan : " + pendapatan); 
			}
			else{
				int DurasiBaru = ((int) (kebahagiaan /bebanKerja));
				bebanKerjaTotal = DurasiBaru *bebanKerja;
				pendapatan = bebanKerjaTotal *10000;
				this.setKebahagiaan(this.kebahagiaan - bebanKerjaTotal);				
				System.out.println(this.nama + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : " + pendapatan);
			}
			this.uang += pendapatan;
		}
			
	}
	
	public void rekreasi (String namaTempat){
		int biaya = namaTempat.length() * 10000;
		if (biaya > this.uang){
			System.out.println(this.nama + " tidak mempunyai cukup uang untuk berekreasi di " + namaTempat + ":(");
		}
		else{
			
			float kebahagiaan_baru = (this.kebahagiaan + namaTempat.length());
			this.setKebahagiaan(kebahagiaan_baru);
			System.out.println(this.nama + " berekreasi di " + namaTempat + ", " + this.nama + " senang :)");
			this.uang-= biaya;
		}
	}
	
	public void sakit(String namaPenyakit){
		float kebahagiaan_baru = this.kebahagiaan - namaPenyakit.length();
		this.setKebahagiaan(kebahagiaan_baru);
		System.out.println(this.nama + " terkena peyakit " + namaPenyakit + " :O");
	}
	
	public String toString(){
		return ("Nama \t \t : " + this.nama + "\nUmur \t \t : " + this.umur + "\nUang \t \t : " + this.uang + "\nKebahagiaan \t : " + this.kebahagiaan);
	}
}

//adykarta