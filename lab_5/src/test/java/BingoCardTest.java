import org.junit.jupiter.api.*;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Scanner;
import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This code is redeveloped from Ichlasul Affan's code
 */

@DisplayName("BingoCard")
public class BingoCardTest {
	// Membuat variabel baru untuk output dan error output
	private ByteArrayOutputStream outContent;
	private ByteArrayOutputStream errContent;
    private BingoCard card;

	// Untuk setiap test, lakukan setup terlebih dahulu
	@BeforeEach
	void init() {
		// make new ByteArrayOutputStream for output and error output
		outContent = new ByteArrayOutputStream();
		errContent = new ByteArrayOutputStream();

		System.setOut(new PrintStream(outContent)); // Connect outContent ke System.out
		System.setErr(new PrintStream(errContent)); // Connect errContent ke System.err

		// inisiasi objek BingoCard
		// untuk inisiasi kalian boleh mengubah bagian kode ini untuk menyesuaikan dengan class BingoCard yang telah kalian buat
        int [][] numbers = {{10,12,14,16,18},
        {29,27,25,23,21},
        {35,34,30,38,39},
        {40,44,45,49,41},
        {52,67,73,81,90};
		
		

        card = new BingoCard(numbers);
		// batas kode yang diperbolehkan untuk diubah
	}	

	@Test
	@DisplayName("Test Info on an unmarked card")
	void tesInfoBersih() {
		assertEquals("| 10 | 12 | 14 | 16 | 18 |\n| 29 | 27 | 25 | 23 | 21 |\n| 35 | 34 | 30 | 38 | 39 |\n| 40 | 44 | 45 | 49 | 41 |\n| 52 | 67 | 73 | 81 | 90 |", card.info());
	}

	@Test
	@DisplayName("Test Mark a number on a card")
	void testMarkSuccess() {
		assertEquals("14 tersilang",card.markNum(14));
        assertEquals("| 10 | 12 | X  | 16 | 18 |\n| 29 | 27 | 25 | 23 | 21 |\n| 35 | 34 | 30 | 38 | 39 |\n| 40 | 44 | 45 | 49 | 41 |\n| 52 | 67 | 73 | 81 | 90 |", card.info());
        assertEquals("10 tersilang",card.markNum(10));
        assertEquals("| X  | 12 | X  | 16 | 18 |\n| 29 | 27 | 25 | 23 | 21 |\n| 35 | 34 | 30 | 38 | 39 |\n| 40 | 44 | 45 | 49 | 41 |\n| 52 | 67 | 73 | 81 | 90 |", card.info());
        assertEquals("49 tersilang",card.markNum(49));
        assertEquals("| X  | 12 | X  | 16 | 18 |\n| 29 | 27 | 25 | 23 | 21 |\n| 35 | 34 | 30 | 38 | 39 |\n| 40 | 44 | 45 | X  | 41 |\n| 52 | 67 | 73 | 81 | 90 |", card.info());
        assertEquals("90 tersilang",card.markNum(90));
        assertEquals("| X  | 12 | X  | 16 | 18 |\n| 29 | 27 | 25 | 23 | 21 |\n| 35 | 34 | 30 | 38 | 39 |\n| 40 | 44 | 45 | X  | 41 |\n| 52 | 67 | 73 | 81 | X  |", card.info());
	}

	@Test
	@DisplayName("Test Mark a marked number on a card")
	void testMarkMultipleOccurence() {
        card.markNum(14);
        assertEquals("| 10 | 12 | X  | 16 | 18 |\n| 29 | 27 | 25 | 23 | 21 |\n| 35 | 34 | 30 | 38 | 39 |\n| 40 | 44 | 45 | 49 | 41 |\n| 52 | 67 | 73 | 81 | 90 |", card.info());
        assertEquals("14 sebelumnya sudah tersilang",card.markNum(14));
        assertEquals("| 10 | 12 | X  | 16 | 18 |\n| 29 | 27 | 25 | 23 | 21 |\n| 35 | 34 | 30 | 38 | 39 |\n| 40 | 44 | 45 | 49 | 41 |\n| 52 | 67 | 73 | 81 | 90 |", card.info());
	}

	@Test
	@DisplayName("Test Mark a number not on a card")
	void testMarkFail() {
        assertEquals("Kartu tidak memiliki angka 11",card.markNum(11));
        assertEquals("| 10 | 12 | 14 | 16 | 18 |\n| 29 | 27 | 25 | 23 | 21 |\n| 35 | 34 | 30 | 38 | 39 |\n| 40 | 44 | 45 | 49 | 41 |\n| 52 | 67 | 73 | 81 | 90 |", card.info());
    }

	@Test
	@DisplayName("Test Restart")
	void testRestart() {
		card.markNum(14);
        card.markNum(10);
        card.markNum(49);
        card.markNum(90);
        card.restart();
		assertEquals("| 10 | 12 | 14 | 16 | 18 |\n| 29 | 27 | 25 | 23 | 21 |\n| 35 | 34 | 30 | 38 | 39 |\n| 40 | 44 | 45 | 49 | 41 |\n| 52 | 67 | 73 | 81 | 90 |", card.info());
        card.markNum(49);
		assertEquals("| 10 | 12 | 14 | 16 | 18 |\n| 29 | 27 | 25 | 23 | 21 |\n| 35 | 34 | 30 | 38 | 39 |\n| 40 | 44 | 45 | X  | 41 |\n| 52 | 67 | 73 | 81 | 90 |", card.info());
	}

	@Test
	@DisplayName("Test Win Vertically")
	void testWinVertically() {
		card.markNum(12);
        card.markNum(27);
        card.markNum(34);
        card.markNum(44);
        card.markNum(67);
        assertEquals(true,card.isBingo());
	}

	@Test
	@DisplayName("Test Win Horizontally")
	void testWinHorizontally() {
		card.markNum(45);
        card.markNum(40);
        card.markNum(41);
        card.markNum(49);
        card.markNum(44);
        assertEquals(true,card.isBingo());
	}

	@Test
	@DisplayName("Test Win Diagonally 1")
	void testWinDiagonal1() {
		card.markNum(90);
        card.markNum(49);
        card.markNum(30);
        card.markNum(27);
        card.markNum(10);
        assertEquals(true,card.isBingo());
	}

	@Test
	@DisplayName("Test Win Diagonally 2")
	void testWinDiagonal2() {
		card.markNum(18);
        card.markNum(44);
        card.markNum(23);
        card.markNum(52);
        card.markNum(30);
        assertEquals(true,card.isBingo());
	}
}
