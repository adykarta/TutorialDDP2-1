package character;
public class Human extends Player{

	public Human(String name, int hp){
		super(name,hp);
		super.setType("Human");
	}
	public void attack(Player enemyName){
		super.attack(enemyName);
	}
	public void eat(Player enemyName){
		super.eat(enemyName);
	}

}
//  write Human Class here