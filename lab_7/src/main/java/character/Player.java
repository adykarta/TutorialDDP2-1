package character;
import java.util.ArrayList;


public class Player{
	private int hp;
	private String name;
	private ArrayList<Player> diet = new ArrayList<Player>();
	private String type;
	private boolean matang;
	private String roar;

	public Player(String name, int hp){ 
		this.name = name;
		this.hp = hp;
	
	}


	public String getName(){
		return this.name;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getRoar(){
		return this.roar;
	}
	public void setRoar(String roar){
		this.roar= roar;
	}
	public int getHp(){
		return this.hp;
	}
	public void setHp(int hp){
		if (hp <=0){
			this.hp = 0;
		}
		else{
			this.hp = hp;
		}
	}

	public String getType(){
		return this.type;
	}

	public void setType(String type){
		this.type=type;
	}

	public ArrayList<Player> getDiet(){
		return this.diet;
	}

	public boolean isMatang(){
		return matang;
	}
	public void setMatang(boolean matang){
		this.matang = matang;
	}

	public String Hidup(){
		if(this.getHp()<=0){
			return "Sudah meninggal dunia dengan damai";
		}
		else{
			return "Masih hidup";
		}

	}

	public void attack(Player enemyName){
		if(enemyName.getType().equals("Magician")){
			enemyName.setHp(enemyName.getHp() -20);
		}
		else{
			enemyName.setHp(enemyName.getHp() -10);
		}
	}

	public String burn(Player enemyName){
		if(enemyName.getHp() == 0){
			enemyName.setMatang(true);
			return "Nyawa " + enemyName.getName() + " 0" +"\ndan matang";
		}
		else{
			if(enemyName.getType().equals("Magician")){
				enemyName.setHp(enemyName.getHp()-20);
			}
			else{
				enemyName.setHp(enemyName.getHp()-10);
			}
			if(enemyName.getHp() == 0){
				enemyName.setMatang(true);
				return "Nyawa " + enemyName.getName() + " 0" +"\ndan matang";
			}
			else{
				return "Nyawa " + enemyName.getName() + " "+ enemyName.getHp();
			}
		}
	}

	public String peopleEaten(){
		String temp1 = "";
		if (diet.size() == 0){
			temp1 = "Belum memakan siapa siapa\n";

		}
		else{
			for(Player a: diet){
				temp1+= "Memakan " + a.getType() + " " + a.getName() + "\n";
			}
		}
		return temp1;
	}

	public boolean canEat(Player enemyName){
		boolean a = false;
		if(this.getType().equals("Human") || this.getType().equals("Magician")){
			if(enemyName.getType().equals("Monster") && enemyName.isMatang()==true){
				a=true;
			}
		}
		else if(this.getType().equals("Monster")){
			if(enemyName.getHp()==0 || enemyName.isMatang()==true){
				a=true;
			}
			
		}
		return a;
	}

	public void eat(Player enemyName){
		if(enemyName.getHp()==0){
			this.setHp(this.getHp()+15);
			diet.add(enemyName);

		}
	}
}
//  write Player Class here
