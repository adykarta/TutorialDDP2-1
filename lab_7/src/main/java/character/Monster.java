package character;
public class Monster extends Player{
	

	public Monster(String name, int hp){
		super(name,hp*2);
		super.setType("Monster");
		super.setRoar("AAAAAAaaaAAAAAaaaAAAAAA");
	}
	public Monster(String name, int hp,String roar){
		super(name,hp*2);
		super.setType("Monster");
		super.setRoar(roar);
	}
	public void attack(Player enemyName){
		super.attack(enemyName);
	}

	public void eat(Player enemyName){
		super.eat(enemyName);
	}

}
//  write Monster Class here