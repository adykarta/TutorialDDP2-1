package character;

public class Magician extends Human{

	public Magician(String name, int hp){
		super(name,hp);
		super.setType("Magician");
	}
	public void attack(Player enemyName){
		super.attack(enemyName);
	}
	public void eat(Player enemyName){
		super.eat(enemyName);
	}
	public String burn(Player enemyName){
		return super.burn(enemyName);

	}
}
//  write Magician Class here