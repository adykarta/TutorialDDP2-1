package xoxo;
import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.exceptions.*;
import xoxo.util.XoxoMessage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import static xoxo.key.HugKey.DEFAULT_SEED;

/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <Muhamad Istiady Kartadibrata>
 */
public class XoxoController {
	private int encryptFile = 1;
	private int decryptFile = 1;
	private String strSeed;
    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }


    /**
     * Main method that runs all the business process.
     */
    public void run() {
    	gui.setDecryptFunction(actions ->decryptNow());
    	gui.setEncryptFunction(actions -> encryptNow());
        
    }

    public void decryptNow(){
        String hugKey = gui.getKeyText();
        String message = gui.getMessageText();
        XoxoDecryption decryptText = new XoxoDecryption(hugKey);
        int seed;
        try{
	        if(gui.getSeedText().equals("DEFAULT_SEED")){
	            seed = DEFAULT_SEED;
	        }
	        else{
	            seed = Integer.parseInt(gui.getSeedText());
	        }
	        String afterDecrypt = decryptText.decrypt(message,seed);
	        createDecryptFile(afterDecrypt);
	    }
	    catch(RangeExceededException e){
	    	gui.appendLog("Seed out of range");
	    }
    }
    public void createDecryptFile(String message) {
        File file = new File("D:\\DDP2\\lab_10\\src\\main\\java\\xoxo\\path\\"
                + decryptFile + ".txt");
        try {
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(message);
            writer.flush();
            writer.close();
            gui.appendLog("Decrypted Text : " + message);
            decryptFile++;
        } catch (IOException e) {
            gui.appendLog("Decryption Gagal");
        }
    }
   
    public void encryptNow(){
        String message = gui.getMessageText();
        String kissKey = gui.getKeyText();
        int seed ;
        XoxoEncryption encryptMessage = null;
        try{
            encryptMessage = new XoxoEncryption(kissKey);
        }
        catch (KeyTooLongException e){
            gui.appendLog("Panjang key tidak boleh >28!");
        }
        catch (IncompatibleClassChangeError e){
            gui.appendLog("Karakter yang dimasukan tidak Valid!");
        }
        XoxoMessage encryptText = null;
        try{
            if (gui.getSeedText().equals("DEFAULT_SEED") || gui.getSeedText().equals("")) {
                seed = DEFAULT_SEED;
                strSeed = "" + seed;
                encryptText = encryptMessage.encrypt(message, seed);
            }else{
                seed = Integer.parseInt(gui.getSeedText());
                strSeed = "" + seed;
                encryptText = encryptMessage.encrypt(message,seed);
            }
        } 
   
        catch (NumberFormatException e){
            gui.appendLog("Seed harus angka");
        } 
        catch (SizeTooBigException e){
            gui.appendLog("Size terlalu besar");
        }
        catch(InvalidCharacterException e){
        	gui.appendLog("Karakter tidak valid");
        }


        createEncryptFile(encryptText, strSeed);
    }

    public void createEncryptFile(XoxoMessage message, String seed) {
        File file = new File("D:\\DDP2\\lab_10\\src\\main\\java\\xoxo\\path\\"
                + encryptFile + ".enc");
        try {
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(message.getEncryptedMessage() + seed);
            writer.flush();
            writer.close();
            gui.appendLog("Encrypted Text : " + message.getEncryptedMessage());
            gui.appendLog("Hug Key : " + message.getHugKey().getKeyString());
            gui.appendLog("Seed : " + seed);
            encryptFile++;
        } catch (IOException e) {
            gui.appendLog("Encryption Gagal");
        }
    }
}