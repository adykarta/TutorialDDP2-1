package xoxo.crypto;

import xoxo.exceptions.RangeExceededException;

/**
 * This class is used to create a decryption instance
 * that can be used to decrypt an encrypted message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoDecryption {

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key string.
     */
    public XoxoDecryption(String hugKeyString) {
        this.hugKeyString = hugKeyString;
    }

    /**
     * Decrypts an encrypted message.
     * 
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     */
    public String decrypt(String encryptedMessage, int seed) {
        if(seed<0 || seed>36){
            throw new RangeExceededException("Seed out of range");
        }
        String result= "";
        //TODO: Implement decryption algorithm
        for (int i = 0; i<encryptedMessage.length(); i ++){
            int z = hugKeyString.charAt(i%hugKeyString.length())^seed;
            int b = z - 'a';
            int x = encryptedMessage.charAt(i)^b;
            result += (char)x;

        }
        return result;
    }
}