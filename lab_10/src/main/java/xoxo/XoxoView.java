package xoxo;

import java.awt.event.ActionListener;
import java.awt.*;
import javax.swing.*;


/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoView {
    
    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    
    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField; 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    //TODO: You may add more components here

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.pilihan();
    }

    public void pilihan(){
        String[] listOptions = {"Mulai", "Exit"};
        int option = JOptionPane.showOptionDialog(null, "Selamat Datang!", "Xoxo!",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, listOptions, listOptions[1]);
        if (option == 0) {
            this.initGui();
        } 
        else {
            exit();
        }
    }
    private void exit(){
        System.exit(0);
    }
    /**
     * Constructs the GUI.
     */

    private void initGui() {
        JFrame window = new JFrame();
        window.setTitle("Enkripsi & Dekripsi");
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel masukan= new JPanel(new GridLayout(3,1));


        JPanel pesan = new JPanel(new FlowLayout());
        messageField = new JTextField();
        messageField.setPreferredSize(new Dimension(200,25));
        JLabel labelPesan = new JLabel("Message : ");
        pesan.add(labelPesan);
        pesan.add(messageField);

        JPanel key = new JPanel(new FlowLayout());
        keyField = new JTextField();
        keyField.setPreferredSize(new Dimension(200,25));
        JLabel labelKey = new JLabel("Key : ");
        key.add(labelKey);
        key.add(keyField);

        JPanel seed = new JPanel(new FlowLayout());
        seedField = new JTextField();
        seedField.setPreferredSize(new Dimension(200,25));
        JLabel labelSeed = new JLabel("Seed : ");
        seed.add(labelSeed);
        seed.add(seedField);

        masukan.add(pesan);
        masukan.add(key);
        masukan.add(seed);

        JPanel tombol = new JPanel(new FlowLayout());
        tombol.setPreferredSize(new Dimension(100,40));

        encryptButton = new JButton("Encrypt");
        encryptButton.setPreferredSize(new Dimension(80,20));
        tombol.add(encryptButton, BorderLayout.LINE_START);

        decryptButton = new JButton("Decrypt");
        decryptButton.setPreferredSize(new Dimension(80,20));
        tombol.add(decryptButton, BorderLayout.LINE_END);

        logField = new JTextArea("Log \n");
        logField.setPreferredSize(new Dimension(200,200));

        window.add(masukan, BorderLayout.PAGE_START);
        window.add(tombol, BorderLayout.CENTER);
        window.add(logField, BorderLayout.PAGE_END);
        window.setVisible(true);
        window.setLocation(500,30);
        window.setSize(500,400);
        window.setResizable(false);
       

    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     * 
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }
}