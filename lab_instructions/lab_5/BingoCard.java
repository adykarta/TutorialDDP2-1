/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */
import java.util.Scanner;
public class BingoCard {
//instance variable
	private int[][] numbers;
	private int[][] numberStates; 
	private boolean isBingo;
//constructor	
	public BingoCard(int[][] numbers) {
		this.numbers = numbers;
		int[][] sim = {
		{0,0,0,0,0},
		{0,0,0,0,0},
		{0,0,0,0,0},
		{0,0,0,0,0},
		{0,0,0,0,0},
		};
		this.numberStates = sim;
		this.isBingo = false;
	}
//method  getter mengambil angka dari baris berapa kolom berapa
	public int getNumbers(int brs, int klm) {
		return numbers[brs][klm];
	}
//method setter 
	public void setNumbers(int[][] numbers) {
		this.numbers = numbers;
	}
//method getter untuk mengambil data bagian mana yana yang sudah tersilang(1) dan yang belom (0)
	public int getNumberStates(int brs, int klm) {
		return numberStates[brs][klm];
	}
//method untuk setter data bagian mana yang sudah tersilang(1) dan yang belom (0)
	public void setNumberStates(int brs, int klm, int ubh) {
		this.numberStates[brs][klm] = ubh;
	}	

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}
//method untuk mengecek bingo	
	public boolean cekBingo(){
		for (int i = 0; i<5; i++){ //ngecek per baris
			int sim = 0;
			for ( int j = 0; j<5; j++){
				sim += getNumberStates(i,j);
			}
			if ( sim == 5){
				return true;
			}
		}
		for (int j = 0; j<5; j++){ //ngecek per kolom
			int sim = 0;
			for (int i = 0; i<5; i++){
				sim += getNumberStates(i,j);
			}
			if ( sim == 5){
				return true;
			}
		}
		int sim = 0;
		for ( int i = 0; i<5; i++){
			sim += getNumberStates(i,i);
		}
		if(sim == 5){
			return true;
		}
		sim = 0;
		for (int i = 0 ; i<5; i++){
			sim +=getNumberStates(i,4-i);
		}
		if (sim ==5){
			return true;
		}
		return false;
	}
//method untuk menandakan bagian yang di mark 
	public String markNum(int num){
		//TODO Implement
		String sim = "";
		for (int i = 0; i<5; i++){
			for (int j = 0; j<5; j++){
				if(getNumbers(i,j) == num){
					if(getNumberStates(i,j) == 1){
						sim = num + " sebelumnya sudah tersilang";
						return sim;
					}
					else{
						sim = num + " tersilang";
					setNumberStates(i, j, 1);
					isBingo = cekBingo();
					return sim;
					}
					
				}
			}
		}
		return "Kartu tidak memiliki angka " + num;
	}	
//method info
	public String info(){
		//TODO Implement
		String sim = "";
		for (int i = 0; i<5; i++){
			for (int j = 0; j<5; j++){
				sim += "| ";
				if(getNumberStates(i,j) == 1){
					sim +="X ";
				}
				else{
					sim +=Integer.toString(getNumbers(i,j));
				}
				sim += " ";
			}
			sim += "| \n";
		}
		return sim;
	}
//method restart game
	public void restart(){
		for (int i = 0; i<5; i++){
			for (int j = 0; j<5; j++){
				this.setNumberStates(i,j,0);
			}
		}
		
		System.out.println("Mulligan!");
	}
//method main
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int[][] sim = new int[5][5];
		for (int i = 0; i<5; i++){
			for (int j = 0; j<5; j++){
				sim[i][j] = input.nextInt();
			}
		}
		BingoCard card = new BingoCard(sim);
		
		while(!card.isBingo()){
			String perintah = input.next();
			if(perintah.equals("MARK")){
				int x = input.nextInt();
				System.out.println(card.markNum(x));
				if(card.cekBingo()){
					System.out.println("BINGO!");
					System.out.print(card.info());
					card.setBingo(true);
				}
			}
			else if(perintah.equals("INFO")){
				System.out.print(card.info());
			}
			else if(perintah.equals("RESTART")){
				card.restart();
			}
			else{
				System.out.println("Masukan Salah!");
			}
		}
	}

}
//adykarta